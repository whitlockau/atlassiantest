package com.atlassian.atlassiantest.chatparser;

import com.google.common.base.Joiner;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.GsonBuilder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Contains functionality for extracting markup from chat messages.
 */
public final class ChatParser {

    /** Key for the JSON property holding 'mentions' from a chat message. */
    public static final String JSON_KEY_MENTIONS = "mentions";

    /** Key for the JSON property holding emoticon references from a chat message. */
    public static final String JSON_KEY_EMOTICONS = "emoticons";

    /** Key for the JSON property holding information about URLs in a chat message. */
    public static final String JSON_KEY_LINKS = "links";

    /** Key for the JSON property holding a URL taken from a chat message. */
    public static final String JSON_KEY_URL = "url";

    /** Key for the JSON property holding the title of a page whose URL is in a chat message. */
    public static final String JSON_KEY_TITLE = "title";

    /** Private ctor to prevent instantiation */
    private ChatParser() {}

    /**
     * A collection of regular expressions which define markup within text messages.
     * <p>
     * Each entry in this map has a value which is a regular expression defining one kind of
     * markup to be extracted when parsing a chat message, and a key which will be used as the
     * property key for the corresponding markup in the JSON produced by parsing.  Each regex
     * must have a single capturing group, which specifies the value of each markup item extracted.
     */
    private static final Map<String, String> MARKUP_SPECS = new TreeMap<>();
    static {
        // Mention: an @ symbol followed by one or more word chars. We require a word break before
        // a mention to distinguish it from an email address.
        MARKUP_SPECS.put(JSON_KEY_MENTIONS, "(?<!\\w)@(\\w+)");

        // Emoticon: 15 or fewer alphanumeric characters enclosed in parentheses.  An emoticon is
        // itself a form of character, so we allow it to be contained within a word (we don't
        // require a word break before or after it).
        MARKUP_SPECS.put(JSON_KEY_EMOTICONS, "\\((\\p{Alnum}{1,15})\\)");

        // Link: the URL of a web page. The complexities of recognizing URLs are discussed at length
        // here: http://www.regexguru.com/2008/11/detecting-urls-in-a-block-of-text/.  This regex is
        // a simplified version of the last regex on that page; we only support http/https URLs
        // since a web page is required (its title will be retrieved).  It could be extended to
        // support other schemes easily enough.
        MARKUP_SPECS.put(JSON_KEY_LINKS, "\\b((?:https?://)" +
                "(?:\\([-\\w+&@#/%=~|$?!:,.]*\\)|[-\\w+&@#/%=~|$?!:,.])*" +
                "(?:\\([-\\w+&@#/%=~|$?!:,.]*\\)|[\\w+&@#/%=~|$]))");
    }

    /**
     * The kinds of markup found by parsing chat messages as a list of JSON property keys, cached
     * in a constant for efficiency.
     */
    private static final String[] MARKUP_KINDS = MARKUP_SPECS.keySet().toArray(new String[0]);

    /**
     * The combined regular expression produced by merging the individual regular expressions for
     * each kind of markup.
     */
    private static final Pattern MARKUP_PATTERN = Pattern.compile("(?:" + Joiner.on(")|(?:").join(MARKUP_SPECS.values()) + ")");

    /**
     * Logger for this class (I would usually use slf4j but for demo purposes java.util.logging will
     * suffice).
     */
    private static final Logger LOG = Logger.getLogger("com.atlassian.atlassiantest.chatparser.ChatParser");

    /**
     * Parses a chat message to extract any markup it may contain.
     * <p>
     * Note that this method may require network I/O (for web page title lookup) and so should not
     * be called on threads where blocking for arbitrary periods presents a problem.
     * <p>
     * The extracted markup is returned as a JSON object (in string form) which may contain the
     * following properties:
     * <ul>
     * <li>"mentions": an array of strings, each of which names a mentioned entity.
     * <li>"emoticons": an array of strings, each of which is an emoticon reference.
     * <li>"links": an array of objects, each of which describes a URL found in the message.  Each
     * of these objects contains a "url" property whose value is the URL itself, and possibly
     * a "title" property containing the title of the page at that URL.
     * </ul>
     * If no markup of a particular kind is found in the message, the corresponding property
     * will be absent.
     * <p>
     * The "title" property for URLs will be missing if the URL did not refer to a web page, or if
     * the page could not be retrieved.  It will contain an empty string if the page was valid but
     * had no title.
     * <p>
     * Examples of the JSON format can be found in the unit test for this class.
     *
     * @param message the chat message to be parsed.
     * @return a string containing any extracted markup, in JSON format.
     */
    public static String parseMessage(String message) {
        LOG.log(Level.FINE, "Parsing chat message '{0}'", message);

        final Multimap<String, Object> markup = ArrayListMultimap.create();

        // Use the markup regex to find all instances of markup in the message.  Each instance
        // is captured in a regex capture group, and the group number tells us the kind of
        // markup it is.
        final Matcher matcher = MARKUP_PATTERN.matcher(message);
        while (matcher.find()) {
            for (int i = 0; i < MARKUP_KINDS.length; ++i) {
                if (matcher.group(i + 1) != null) {
                    markup.put(MARKUP_KINDS[i], matcher.group(i + 1));
                }
            }
        }

        // Perform any post-processing required for the markup.
        postProcessMarkup(markup);

        // Convert the markup object to a JSON string.
        String markupJson = new GsonBuilder().create().toJson(markup.asMap());

        LOG.log(Level.FINE, "Resulting markup is '{0}'", markupJson);
        return markupJson;
    }

    /**
     * Performs post-processing on markup extracted from a chat message.
     * <p>
     * Currently this just means retrieving the page titles for any links that appeared in the
     * message.
     *
     * @param markup the parsed markup from a chat message.
     */
    private static void postProcessMarkup(Multimap<String, Object> markup) {

        final Collection<Object> links = markup.get(JSON_KEY_LINKS);
        if (links != null) {

            // For each link in the markup, fetch the title of the page to which it refers, and
            // create an object containing both the URL and the title.
            final List<Object> linksWithTitles = new ArrayList<>();
            for (final Object link: links) {
                try {
                    // Use Jsoup to fetch and parse the document. This does more work than we really
                    // need (it creates a DOM for the whole document) but the code is trivial - hand-
                    // crafting our own code to do it would be a premature optimisation at this point.
                    final Document document = Jsoup.connect((String) link).get();

                    // If the document has no title, the link object will contain a "title" property
                    // with the empty string as its value.
                    linksWithTitles.add(createLinkObject(link, document.title()));

                } catch (IOException e) {
                    LOG.log(Level.FINE, "Unable to retrieve page title: {0}", e);

                    // We could not obtain a title for the document (either it doesn't exist,
                    // couldn't be fetched, or wasn't a valid HTML doc), so the link object will
                    // not contain a "title" property.
                    linksWithTitles.add(createLinkObject(link, null));
                }
            }

            // Replace the raw link strings with the link objects we have created.
            markup.replaceValues(JSON_KEY_LINKS, linksWithTitles);
        }
    }

    /**
     * Helper method which creates a "link" object for the parsed markup of a chat message.
     *
     * @param url the value of the "url" property in the object to be created.
     * @param title the value, if any, of the "title" property in the object to be created.
     * @return a link object containing the given url and (if not null) title.
     */
    private static Object createLinkObject(Object url, String title) {
        final Map<String, Object> result = new TreeMap<>();
        result.put(JSON_KEY_URL, url);
        if (title != null) {
            result.put(JSON_KEY_TITLE, title);
        }
        return result;
    }
}
