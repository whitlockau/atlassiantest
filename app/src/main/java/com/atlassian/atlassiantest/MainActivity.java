package com.atlassian.atlassiantest;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.atlassian.atlassiantest.chatparser.ChatParser;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The application's main Activity.
 */
public class MainActivity extends AppCompatActivity {

    /** Worker thread for performing tasks which can't be done on the GUI thread. */
    private ExecutorService mWorkerThread = Executors.newSingleThreadExecutor();

    /** Handler for executing tasks on the GUI thread. */
    private Handler mGuiHandler = new Handler(Looper.getMainLooper());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // When the user finishes entering text in the input EditText, parse it and show the markup.
        final EditText inputText = (EditText) findViewById(R.id.inputText);
        inputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView self, int actionId, KeyEvent event) {
                showChatMarkup(inputText.getText().toString());
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_test_input1 || id == R.id.action_test_input2
                || id == R.id.action_test_input3 || id == R.id.action_test_input4) {

            // The user has selected a test string - populate the chat EditText and the markup TextView.
            final EditText inputText = (EditText) findViewById(R.id.inputText);
            inputText.setText(item.getTitle());
            showChatMarkup(item.getTitle().toString());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Helper function to extract and display markup from a chat message.
     *
     * @param chatMessage the chat message whose markup is to be displayed.
     */
    private void showChatMarkup(final String chatMessage) {

        final TextView resultText = (TextView) findViewById(R.id.resultText);
        resultText.setText("Processing ...");

        // Parsing the message could involve network lookup, so it shouldn't be done on the GUI
        // thread - kick it over to our worker thread.
        mWorkerThread.submit(new Runnable() {
            @Override
            public void run() {
                final String json = ChatParser.parseMessage(chatMessage);
                final String prettyJson = new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse(json));

                // Now that we have the resulting markup, head back to the GUI thread to show it.
                mGuiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        resultText.setText(prettyJson);
                    }
                });
            }
        });
    }
}
