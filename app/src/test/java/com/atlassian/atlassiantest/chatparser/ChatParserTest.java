package com.atlassian.atlassiantest.chatparser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import junit.framework.TestCase;

/**
 * Unit tests for the ChatParser class.
 */
public class ChatParserTest extends TestCase {

    public void testParseMessage_simpleMention() throws Exception {
        parseAndValidate("@chris you around?", "{\"mentions\": [\"chris\"]}");
    }

    public void testParseMessage_simpleEmoticons() throws Exception {
        parseAndValidate("Good morning! (megusta) (coffee)", "{\"emoticons\": [ \"megusta\", \"coffee\" ]}");
    }

    public void testParseMessage_simpleLink() throws Exception {
        // Note that in a real unit test, we wouldn't rely on external web sites, we would use a
        // test server under our own control.
        final String expected = "{\"links\": [ { \"url\": \"http://www.nbcolympics.com\"," +
                " \"title\": \"NBC Olympics | Home of the 2016 Olympic Games in Rio\" } ]}";
        parseAndValidate("Olympics are starting soon; http://www.nbcolympics.com", expected);
    }

    public void testParseMessage_allMarkup() throws Exception {
        final String input = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016";
        final String expected = "{ \"mentions\": [ \"bob\", \"john\" ]," +
                "  \"emoticons\": [ \"success\" ]," +
                "  \"links\": [ {" +
                "      \"url\": \"https://twitter.com/jdorfman/status/430511497475670016\"," +
                "      \"title\": \"Justin Dorfman on Twitter: \\\"nice @littlebigdetail from @HipChat" +
                " (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\\\"\" }]}";
        parseAndValidate(input, expected);
    }

    public void testParseMessage_nonMatches() throws Exception {
        parseAndValidate("no matches for email address foo@bar.com, too-long emoticon (sixteencharslong)", "{}");
    }

    public void testParseMessage_parentheses() throws Exception {
        String input = "emoticon in (poo)tastic, expiala(doh)cious, none in https://en.wikipedia.org/wiki/Interpol_(band)";
        String expected = "{\"emoticons\": [\"poo\", \"doh\"], \"links\":[" +
                "{\"title\":\"Interpol (band) - Wikipedia, the free encyclopedia\"," +
                " \"url\":\"https://en.wikipedia.org/wiki/Interpol_(band)\"}]}";
        parseAndValidate(input, expected);
    }

    public void testParseMessage_linkWithoutTitle() throws Exception {
        String input = "nonexistent link http://nothing.example.com should not have 'title' property";
        parseAndValidate(input, "{\"links\"=[{ \"url\"=\"http://nothing.example.com\" }]}");
    }

    /**
     * Passes the given message to the ChatParser.parseMessage method, then tests the result against
     * the given expectedJson value.
     *
     * @param message the message to be parsed.
     * @param expectedJson the JSON we expect to get as the result.
     * @throws AssertionError if the result does not match the expected value.
     */
    private void parseAndValidate(String message, String expectedJson) {
        String actualJson = ChatParser.parseMessage(message);

        // We compare the JSON as objects to decrease fragility due to things like property
        // ordering, whitespace etc.
        final Gson gson = new GsonBuilder().create();
        Object expectedJsonObject = gson.fromJson(expectedJson, Object.class);
        Object actualJsonObject = gson.fromJson(actualJson, Object.class);
        assertEquals(expectedJsonObject, actualJsonObject);
    }
}
